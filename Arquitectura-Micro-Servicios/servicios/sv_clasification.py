# -*- coding: utf-8 -*-
# !/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------
# Archivo: sv_clasification.py
# Tarea: 2 Arquitecturas Micro Servicios.
# Autor: StarLab
#   -Brenda Yasmin Barrios Becerra.
#   -Paola Torres Macías.
#   -Mario Alberto Muro Barraza.
# Version: 1.9 Mayo 2019
# Descripción:
#
#   Este archivo define el rol de un servicio. Su función general es obtener la clase (positivo:pos, negatico:neg o  neutral:neutral)
#	de cada uno de los comentarios encontrados.s
#   
#
#
#
#                                        sv_clasification.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |  - Ofrecer un JSON que  | - Realiza una coneccion|
#           |    Clasificador de    |    contenga los valores |   y consulta a una     | 
#           |    comentarios        | 	 de los comentarios de|   aplicación (text-pro |
#           |    de twitter         |    twitter              |   cessing) para valorar|
#           |                       |                         |   cada tweet.          |
#           |                       |                         | - Devuelve un JSON con |
#           |                       |                         |   las valoraciones     |
#           |                       |                         |   generales de todos   |
#			|						|	                      |   tweets.              |
#           +-----------------------+-------------------------+------------------------+
#                          
#	Ejemplo de uso: Abrir navegador e ingresar a http://localhost:8002/api/v3/information?t=superman

from flask import Flask, abort, render_template, request
import json
import os
import requests
import unicodedata

#definimos el lenguaje para la clasificacion
app = Flask(__name__)

#esta funcion devuelve si un texto es positivo, negativo o neutral
def analize(text): 
	#se crea un JSON que contiene el texto (texto del tweet), necesario para interactuar con la API
	data = {'language':'english','text':text}
	#Se manda a llamar a la página (text-processing) junto con el texto
	json_response =  requests.post('http://text-processing.com/api/sentiment/', data=data).text
	# Se convierte en un JSON la respuesta recibida
	response = json.loads(json_response)
	# Se regresa el atributo del JSON ocn la clase del comentario
	return response['label']

@app.route("/api/v1/clasification", methods=['POST'])
def get_information():
	#Se recive el json enviado a travéz del POST
	cmnts = request.get_json()
	#Inicialización de variables para valoración de sentimientos de la película
	p=n=nt=0.0
	new_cmnts=[]
	#Recuperación de todos los tweets del JSON recibido por POST
	for tweet in cmnts:
		#uname = unicodedata.normalize('NFKD', tweet["User Name"]).encode('ascii','ignore')
		#Se obtiene el texto de cada tweet y se le aplica un tipo de codificación para leer carácteres especiales
		txt = unicodedata.normalize('NFKD', tweet["Tweet Text"]).encode('ascii','ignore')
		#Se llama a la función que analiza el sentimiento de cada tewwt
		clase = analize(txt)
		
		#Recopilación de tipo de cada tweet
		if clase == "pos":
			p+=1.0
		elif clase == "neg":
			n+=1.0
		else:
			nt+=1.0
	#Total de tweets
	tot = nt+n+p
	#Se genera un diccionario con las estadísticas finales de todos los tweets
	res={
		'sentimientos' : {
			'pos':100*(p/tot),
			'neg':100*(n/tot),
			'neutral':100*(nt/tot)
		}
	}	
	#Se regresa el resultado en formato JSON
	return json.dumps(res), 200


if __name__ == '__main__':
    # Se define el puerto del sistema operativo que utilizará el servicio
    port = int(os.environ.get('PORT', 8082))
    # Se habilita la opción de 'debug' para visualizar los errores
    app.debug = True
    # Se ejecuta el servicio definiendo el host '0.0.0.0' para que se pueda acceder desde cualquier IP
    app.run(host='0.0.0.0', port=port)