# -*- coding: utf-8 -*-
# !/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------
# Archivo: sv_coments.py
# Tarea: 2 Arquitecturas Micro Servicios.
# Autor: StarLab
#   -Brenda Yasmin Barrios Becerra.
#   -Paola Torres Macías.
#   -Mario Alberto Muro Barraza.
# Version: 1.9 Mayo 2019
# Descripción:
#
#   Este archivo define el rol de un servicio. Su función general es porporcionar en un objeto JSON
#   una cantidad determinada de TWEETS relacionados al título de la película a travéz de la
#   librería tweepy.
#
#
#
#                                        sv_coments.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |  - Ofrecer un JSON que  | - Utiliza lalibrería de|
#           |    Procesador de      |    contenga tweets      |   tweepy.              |
#           |     comentarios       |    relacionados con la  | - Devuelve un JSON con |
#           |     de Twitter        |    película.            |   los comentarios de   |
#           |                       |                         |   twitter.             |
#           +-----------------------+-------------------------+------------------------+
#
                       

import tweepy
import os
from flask import Flask, abort, render_template, request
import urllib, json

#Guardamos llaves en constantes
CONSUMERKEY = "vHX6fdHT8HyDCDZZz7Zj2r842"#"UrKYBQjgYs8YKoTvTLPrkIZum"
CONSUMERSECRET = "61q5FrV2kCqLWJsDiwFMwo2IKmKhTPs39GiabhipIJW9exDlhn"#"JgOQnKKRxbJRzRBpcYuCYGiMNHqbOWKkEywnpMGyulGSGTJPgR"
ACCESTOKEN = "139508564-Gxo1cvJij56nAsm8eNgdR64Ubje13a2Nl9DUqigy"#"2858235567-4SeI6G7hGKeomWFlS99jIPGQXlsT0dBVNJhCGMN"
ACCESTOKENSECRET = "NSCyuWHOO72k3WA1UURdeXz1XRv0a4aUxVHt1QtwZ6UwS"#"jzG0nOonMaMyWtZ2yUX7vBvHDqHkcrIfcfDYlSTnkSYw3"

app = Flask(__name__)

@app.route("/api/v1/coments", methods=['GET'])
def get_tweets():
	#Usamos las llaves para genetrar el auth
	auth = tweepy.OAuthHandler(CONSUMERKEY, CONSUMERSECRET)
	auth.set_access_token(ACCESTOKEN, ACCESTOKENSECRET)
	#Nos identificamos en la api de twitter con las llaves
	api = tweepy.API(auth)
	#Obtenemos el nombre de la consulta a realizar
	query = request.args.get("t")
	#Definimos el numero maximo de tweets a guardar
	max_tweets = 100
	#Buscamos comentarios sobre la pelicula
	searched_tweets = []
	for tweet in tweepy.Cursor(api.search, q=query, lang='en').items(max_tweets):
		twt = {
			'Tweet Id': tweet.id_str.encode('utf-8'),
            'User Name': tweet.user.name.encode('utf-8'),
            'Tweet Text': tweet.text.encode('utf-8')
            }
		searched_tweets.append(twt)
	#En caso de encontrarse tweets relacionados se regresan, si no se regresa el código de error
	if len(searched_tweets)!=0:
		return json.dumps(searched_tweets),200
	else:
		return ('', 204)

if __name__ == '__main__':
    # Se define el puerto del sistema operativo que utilizará el servicio
    port = int(os.environ.get('PORT', 8083))
    # Se habilita la opción de 'debug' para visualizar los errores
    app.debug = True
    # Se ejecuta el servicio definiendo el host '0.0.0.0' para que se pueda acceder desde cualquier IP
    app.run(host='0.0.0.0', port=port)


