# -*- coding: utf-8 -*-
# !/usr/bin/env python
# ----------------------------------------------------------------------------------------------------------------
# Archivo: gui.py
# Tarea: 2 Arquitecturas Micro Servicios.
# Autor: StarLab
#   -Brenda Yasmin Barrios Becerra.
#   -Paola Torres Macías.
#   -Mario Alberto Muro Barraza.
# Autores Iniciales: Perla Velasco & Yonathan Mtz.
# Version: 1.9 Mayo 2019
# Descripción:
#
#   Este archivo define la interfaz gráfica del usuario. Recibe dos parámetros que posteriormente son enviados
#   a servicios que la interfaz utiliza.
#   
#   
#
#                                             gui.py
#           +-----------------------+-------------------------+------------------------+
#           |  Nombre del elemento  |     Responsabilidad     |      Propiedades       |
#           +-----------------------+-------------------------+------------------------+
#           |                       |  - Porporcionar la in-  | - Consume servicios    |
#           |          GUI          |    terfaz gráfica con la|   para proporcionar    |
#           |                       |    que el usuario hará  |   información al       |
#           |                       |    uso del sistema.     |   usuario.             |
#           +-----------------------+-------------------------+------------------------+
#
import os
from flask import Flask, render_template, request
import urllib, json
import requests


app = Flask(__name__)
comments = {}

@app.route("/")
def index():
    # Método que muestra el index del GUI
    return render_template("index.html")

@app.route("/information", methods=['GET'])
def sentiment_analysis():
    # Se obtienen los parámetros que nos permitirán realizar la consulta
    title = request.args.get("t")
    if len(title) is not 0:
        # La siguiente url es para un servicio local
        url_omdb = urllib.urlopen("http://127.0.0.1:8084/api/v1/information?t=" + title)
        # Se lee la respuesta de OMDB
        json_omdb = url_omdb.read()
        # Se convierte en un JSON la respuesta leída
        omdb = json.loads(json_omdb)

        #Se intenta concatenar el tipo de lo que se busca (película o serie) con el título
        try:
            #Generamos un argumento más específico para la busqueda de comentarios (usamos el tipo de film que se busca, serie o peícula)
            arrg = omdb["Type"]+" "+title
        #En caso de que no se encuentre la película
        except KeyError:
            arrg = title
        #Se manda a buscar en los comentarios las coincidencias con el título del film y el tipo
        url_comments = urllib.urlopen("http://127.0.0.1:8083/api/v1/coments?t="+arrg)

        #Se lee la respuesta y se convierte en un JSON la respuesta leída. Depsués...
        #se llama al micro servicio (por un POST) encargado de obtener el análisis...
        # de sentimientos mandandelo el JSON.
        url_cmnts_valortion = requests.post("http://0.0.0.0:8082/api/v1/clasification",json = json.loads(url_comments.read()))

        # Se lee la respuesta y luego se convierte en un JSON
        cmnt_val = json.loads(url_cmnts_valortion.text)


        # Se llena el JSON que se enviará a la interfaz gráfica
        json_result = {
            'omdb': omdb,
            'cmnts_val' : cmnt_val
        }
            
        # Se regresa el template de la interfaz gráfica predefinido así como los datos que deberá cargar
        return render_template("status.html", result=json_result)
    else:
        return render_template("error-500.html")


if __name__ == '__main__':
    # Se define el puerto del sistema operativo que utilizará el Sistema de Procesamiento de Comentarios (SPC).
    port = int(os.environ.get('PORT', 8000))
    # Se habilita el modo debug para visualizar errores
    app.debug = True
    # Se ejecuta el GUI con un host definido cómo '0.0.0.0' para que pueda ser accedido desde cualquier IP
    app.run(host='0.0.0.0', port=port)